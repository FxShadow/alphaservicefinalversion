class ApplicationController < ActionController::Base
    before_action :configure_permitted_parameters, if: :devise_controller?

    protected
  
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :last_name, :email, :identity_document, :type_document, :phone, :municipality, :address])
      devise_parameter_sanitizer.permit(:account_update, keys: [:name, :last_name, :email, :identity_document, :type_document, :phone, :municipality, :address])
    end

    def after_sign_out_path_for(resource)
      mainpage_url
    end

    rescue_from CanCan::AccessDenied do
      flash[:error] = 'No tienes los permisos para acceder a este apartado!'
      redirect_to root_url
    end

end
