# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)

    if user.administrador?
      can :manage, User
      can :read, Hour
    elsif user.docente? # PENDIENTE DE CAMBIO POR LAS HORAS ASOCIADAS A OTROS USUARIOS
      can :read, Hour 
      can :create, Hour
      can :update, Hour #do |hour|
        #hour.try(:user) == user
      #end
      can :destroy, Hour #do |hour|
        #hour.try(:user) == user
      #end
    elsif user.estudiante?
      can :read, Hour
    end
  end

end
