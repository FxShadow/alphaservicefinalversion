class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  belongs_to :role, optional: true
  has_many :items, dependent: :destroy
  validates :name, presence: true
  before_save :assign_role

  def assign_role
    self.role = Role.find_by name: 'Estudiante' if role.nil?
  end

  def administrador?
    role.name == 'Administrador'
  end

  def docente?
    role.name == 'Docente'
  end

  def estudiante?
    role.name == 'Estudiante'
  end
    
end
