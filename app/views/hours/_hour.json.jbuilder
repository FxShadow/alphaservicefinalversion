json.extract! hour, :id, :user_id, :number_hours, :observations, :created_at, :updated_at
json.url hour_url(hour, format: :json)
