json.extract! profesore, :id, :deparamento, :salon, :nombre, :created_at, :updated_at
json.url profesore_url(profesore, format: :json)
