json.extract! user, :id, :role_id, :name, :last_name, :email, :identity_document, :type_document, :phone, :municipality, :address, :password_digest, :created_at, :updated_at
json.url user_url(user, format: :json)
