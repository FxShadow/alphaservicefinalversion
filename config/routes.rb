Rails.application.routes.draw do
  resources :profesores
  devise_for :users
  scope '/administrador' do
    resources :users
  end

  get 'inicio', to: 'homepage#index', as: 'mainpage'
  get 'informacion', to: 'information#index', as: 'info'
  get 'seguimiento', to: 'follow#index', as: 'seg_info'
  get 'registro_horas', to: 'hours#index', as: 'hours_show'
  get 'registro_horas/editar/:id', to: 'hours#edit', as: 'hours_edit'
  get '/administrador/users', to: 'users#index', as: 'user_show'
  
  resources :hours
  resources :users
  resources :roles

  root to: 'profile#index'
end
