class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.belongs_to :role, null: false, foreign_key: true
      t.string :name
      t.string :last_name
      t.string :email
      t.integer :identity_document
      t.string :type_document
      t.integer :phone
      t.string :municipality
      t.string :address
      #t.string :password_digest

      t.timestamps
    end
  end
end
