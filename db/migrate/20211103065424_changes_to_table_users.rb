class ChangesToTableUsers < ActiveRecord::Migration[6.1]
  def change
    #remove_column :users, :password_digest, :string
    change_column :users, :identity_document, :string
    change_column :users, :phone, :string
  end
end
