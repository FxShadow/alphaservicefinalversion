rol_estudiante = Role.create({ name: 'Estudiante', description: 'Puede ver, modificar sus propios datos y visualizar sus horas' })
rol_docente = Role.create({ name: 'Docente', description: 'Puede realizar cualquier tipo de accion dentro del sistema, a excepcion de modificar los datos propios del usuario' })
rol_superAdministrador = Role.create({ name: 'Administrador', description: 'Este usuario puede crear y modificar usuarios ya existentes dentro del sitio' })

superUsuario_prueba = User.create({ name: 'Super Admin', last_name: 'Del Sitio', email: 'root.alphaservice@gmail.com',
    identity_document: 'N/A', type_document: 'N/A', phone: 'N/A', municipality: 'N/A', address: 'N/A', password: 'rootAlpha', password_confirmation: 'rootAlpha', role_id: 3})

usuario_prueba_1 = User.create({ name: 'Sebastian', last_name: 'Vasquez Parra', email: 'svasquez68@misena.edu.co',
    identity_document: '1035971586', type_document: 'TI', phone: '3216098436', municipality: 'Itagui', address: 'Calle 75A #48-58', password: '123456', password_confirmation: '123456', role_id: rol_docente.id })

usuario_prueba_2 = User.create({ name: 'Juan Sebastian', last_name: 'Posada Rodriguez', email: 'jsposada547@misena.edu.co',
    identity_document: '1025642745', type_document: 'TI', phone: '3156765338', municipality: 'Bello', address: 'Cra. 45 ## 26-162', password: '1234567', password_confirmation: '1234567', role_id: rol_estudiante.id })

usuario_prueba_3 = User.create({ name: 'Samuel', last_name: 'Ramirez Torres', email: 'sramirez000@misena.edu.co',
    identity_document: '1036448000', type_document: 'TI', phone: '3052451232', municipality: 'Envigado', address: 'Carrera 39 N35 Sur 62', password: '12345678', password_confirmation: '12345678', role_id: rol_estudiante.id })

hora_prueba_1 = Hour.create({ number_hours: 40, observations: 'El estudiante Samuel Ramirez realizo estas 40 horas durante el transcurso de la semana del 2 al 7 de Agosto', user_id: usuario_prueba_3.id })
hora_prueba_2 = Hour.create({ number_hours: 10, observations: 'El estudiante Juan Sebastian Posada realizo estas 10 horas durante el transcurso del fin de semana del 6 al 8 de Agosto', user_id: usuario_prueba_2.id })
hora_prueba_3 = Hour.create({ number_hours: 2, observations: 'El estudiante Juan Sebastian Posada realizo estas 2 horas durante el transcurso del dia 15 de Agosto', user_id: usuario_prueba_2.id })